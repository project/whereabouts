(function ($, Drupal) {

  Drupal.behaviors.whereaboutsScrollbarWidthCSS = {
    attach: function (context, settings) {
      function _calculateScrollbarWidth() {
        document.documentElement.style.setProperty('--scrollbar-width', (window.innerWidth - document.documentElement.clientWidth) + "px");
      }
      window.addEventListener('resize', _calculateScrollbarWidth, false);
      document.addEventListener('DOMContentLoaded', _calculateScrollbarWidth, false);
      _calculateScrollbarWidth();
    }
  }

  Drupal.behaviors.whereaboutsWatchTourHash = {
    attach: function (context, settings) {
      let $hero = $(".main-content__hero").once('main-content__hero--watch-hash-processed');
      if ($hero.length < 1) {
        return;
      }

      $(window).on('hashchange', function(e) {
        let $stops = $('.field-item--paragraph--type-stop');

        let reg = /tour-map-stop-(\d+)/gi
        let matches = reg.exec(window.location.hash)
        if (matches != null) {
          window.location.hash = '';
          let target_idx = parseInt(matches[1]) - 1;

          if (!$stops[target_idx]) {
            return;
          }
          window.scrollTo({
            top: $stops[target_idx].getBoundingClientRect().top + window.scrollY + ($hero.hasClass('main-content__hero--open') ? -600 : -200),
            behavior: 'smooth'
          });

        }

      });
    }
  }

  Drupal.behaviors.whereaboutsTrackScrolledStop = {
    attach: function (context, settings) {
      let $tour_map = $('.whereabouts-tour-map-block').once('whereabouts-tour-map-block--processed');
      let $hero = $(".main-content__hero");

      if ($tour_map.length < 1) {
        return;
      }

      let $stops = $('.field-item--paragraph--type-stop');
      let stops_and_y_pos;

      let setup_stops = function() {
        stops_and_y_pos = [];
        $stops.each(function(idx, el) {
          stops_and_y_pos.push({y_top: el.getBoundingClientRect().top + window.scrollY, y_bottom: el.getBoundingClientRect().bottom + window.scrollY, idx: idx});
        })
      }
      setup_stops();
      let resizing = false;
      $(window).on('resize', function() {
        if (resizing) {
          return;
        }
        resizing = true;
        setup_stops();
        setTimeout(() => {
          resizing = false;
          setup_stops();
        }, 50);
      })


      let update_current_stop = function() {
        let current_stop = stops_and_y_pos.find(s => s.y_bottom > (window.scrollY + ($hero.hasClass('main-content__hero--open') ? 600 : 200)));
        $tour_map.attr('current_stop', current_stop.idx);
      }
      let scrolling = false;
      update_current_stop();
      $(document).on('scroll', function() {
        if (scrolling) {
          return;
        }
        scrolling = true;
        update_current_stop();
        setTimeout(() => {
          update_current_stop();
          scrolling = false;
        }, 25);
      })

    }
  }

  Drupal.behaviors.whereaboutsCollapsableTourMap = {
    attach: function (context, settings) {

      let $hero = $(".main-content__hero").once('main-content__hero--processed');


      if ($hero.length < 1) {
        return;
      }
      if ($hero.find('.block-whereabouts-tour-map-block').length < 1) {
        return;
      }

      let heroPos;
      let updateHeroPos = () => {
        heroPos = $hero[0].getBoundingClientRect().top;
      }
      updateHeroPos();
      let resizing = false;
      $(window).on('resize', function() {
        if (resizing) {
          return;
        }
        resizing = true;
        updateHeroPos();
        setTimeout(() => {
          updateHeroPos();
          resizing = false;
        }, 50);
      })

      let opener = $('<button class="hero-expander"><span class="hero-expander__icon"></span><span class="hero-expander__collapse">Collapse Map</span><span class="hero-expander__expand">Expand Map</span></button>');
      $hero.append(opener);

      opener.on('click', function() {
        $hero.toggleClass('main-content__hero--open');
      })

      let update_scrolled = function() {
        if (window.scrollY > heroPos) {
          $hero.css('max-height', Math.max(500 - (window.scrollY - heroPos), 100))
          $hero.addClass('main-content__hero--scrolled');
        }
        else {
          $hero.removeClass('main-content__hero--scrolled');
        }
      }
      let scrolling = false;
      update_scrolled();
      $(document).on('scroll', function() {
        if (scrolling) {
          return;
        }
        scrolling = true;
        update_scrolled();
        setTimeout(() => {
          update_scrolled();
          scrolling = false;
        }, 25);
      })

    }
  }

  Drupal.behaviors.odMobileMenu = {
    attach: function (context, settings) {
      let $main_menu = $('#block-roamer-main-menu ul.menu');
      $main_menu.once('main-menu--processed').each( function () {
        $account_menu_items = $('.menu--account li').clone();
        $account_menu_items.each( function () {
          $(this).addClass('mobile-only');
          $main_menu.append(this);
        });
      });
      let $opener = $('.menu__mobile-opener');
      $opener.once('menu-opener-processed').click(function(e) {
        if (!e.target.matches('.menu__mobile-opener')) return;
        e.preventDefault();
        $opener.toggleClass('menu__mobile-opener--open')
      })
    }
  }

  Drupal.behaviors.whereaboutsVideoWrapper = {
    attach: function attach(context, settings) {
      var $iframes = $('.main-content__content iframe[src*="youtube"], .main-content__content iframe[src*="youtu.be"], .main-content__content iframe[src*="vimeo."]');
      $iframes.not('.video-wrapper-processed').addClass('video-wrapper-processed').wrap('<span class="video-wrapper"></span>');
    }
  };

})(jQuery, Drupal);
