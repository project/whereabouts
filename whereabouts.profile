<?php

/**
 * @file
 * Enables modules and site configuration for the whereabouts installation profile.
 */

use Drupal\Core\Config\FileStorage;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_install_tasks().
 */
function whereabouts_install_tasks() {
  $tasks = [];
  $tasks['whereabouts_post_install_tasks'] = [
    'display_name' => t('Whereabouts installation tasks'),
    'type' => 'normal',
  ];
  return $tasks;
}

function whereabouts_post_install_tasks() {

  $config_path = \Drupal::service('extension.list.profile')
      ->getPath('whereabouts') . '/config/post-install';
  $source = new FileStorage($config_path);
  $config_storage = \Drupal::service('config.storage');

  foreach ($source->listAll() as $item) {
    $config_storage->write($item, $source->read($item));
  } // Loop thru items.

  drupal_flush_all_caches();
  \Drupal::messenger()
    ->addStatus(t('Whereabouts installation tasks complete.'));

}

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 */
function whereabouts_form_install_configure_form_alter(&$form, FormStateInterface $form_state) {
  $form['admin_account']['account']['name']['#default_value'] = 'sysadmin';
}
