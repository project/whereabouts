(function ($, Drupal) {
  Drupal.behaviors.whereaboutsAdmin = {
    attach: function (context, settings) {
      $('.field--name-field-geolocation').once('geolocation--processed').each( function () {

        let $map = $(this);
        let fieldSelector = "input[name=field_geocoding_method]";
        let geocodingMethod = $(fieldSelector);
        if ($(fieldSelector + ':checked').val() === '0') {
          $map.hide();
        }

        geocodingMethod.on('change', function () {
          if ($(fieldSelector + ':checked').val() === '0') {
            $map.hide();
          }
          else {
            $map.show();
            // Resize to make the map display correctly.
            setTimeout(function () {
              window.dispatchEvent(new Event('resize'));
            }, 500);
          }
        });

      });
    }
  };
})(jQuery, Drupal);
