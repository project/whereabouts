(function ($, Drupal) {
  Drupal.behaviors.whereaboutsTourStart = {
    attach: function (context, settings) {
      $('.tour-button, .js-tour-start-button button').once('tour-button--processed').each( function () {

        if (settings.whereabouts_system.hide_tour) {
          $(this).hide();
          $('.tour-toolbar-tab .toolbar-icon-help').hide();
          return;
        }

        let tourName = settings.whereabouts_system.tour_name;
        if (!tourName) {
          return;
        }
        $(this).on('click', function() {
          // Open all details elements when the tour starts.
          $('details summary[aria-pressed=false]').click();
        });

        let cookie_name = 'tour_viewed__' + tourName;
        let tour_viewed = get_cookie(cookie_name);
        if (tour_viewed) {
          return;
        }
        set_cookie(cookie_name, 1, 3650);

        // Start the tour after a short delay.
        let tour_link = this;
        setTimeout(function () {  $(tour_link).click(); }, 3000);

      });
    }
  };
})(jQuery, Drupal);
