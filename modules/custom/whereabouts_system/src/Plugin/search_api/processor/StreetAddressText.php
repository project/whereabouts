<?php

namespace Drupal\whereabouts_system\Plugin\search_api\processor;

use Drupal\Core\Entity\EntityInterface;
use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Processor\ProcessorProperty;

/**
 *
 * @SearchApiProcessor(
 *   id = "street_address_text",
 *   label = @Translation("Street address text"),
 *   description = @Translation("Street number and street name together."),
 *   stages = {
 *     "add_properties" = 0,
 *   },
 *   locked = true,
 *   hidden = false,
 * )
 */
class StreetAddressText extends ProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions(DatasourceInterface $datasource = NULL) {
    $properties = [];

    if (!$datasource) {
      $definition = [
        'label' => $this->t('Street address text'),
        'description' => $this->t('Street number and street name together.'),
        'type' => 'string',
        'processor_id' => $this->getPluginId(),
      ];
      $properties['street_address_text'] = new ProcessorProperty($definition);
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function addFieldValues(ItemInterface $item) {

    /* @var \Drupal\node\Entity\Node $entity */
    $entity = $item->getOriginalObject()->getValue();
    if (!$entity->hasField('field_street_name')) {
      return;
    }

    if (!$street_address = whereabouts_system_build_street_address_text($entity)) {
      return;
    }

    $fields = $this->getFieldsHelper()->filterForPropertyPath($item->getFields(), NULL, 'street_address_text');
    foreach ($fields as $field) {
      $field->addValue($street_address);
    }

  }

}
