<?php

namespace Drupal\whereabouts_system\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an add content links block.
 *
 * @Block(
 *   id = "whereabouts_system_add_content_links",
 *   admin_label = @Translation("Add Content Links"),
 *   category = @Translation("Whereabouts")
 * )
 */
class AddContentLinksBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a new AddContentLinksBlock instance.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, RendererInterface $renderer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   *
   * Based on \Drupal\node\Controller\NodeController::addPage().
   */
  public function build() {

    $definition = $this->entityTypeManager->getDefinition('node_type');
    $build = [
      '#theme' => 'item_list',
      '#cache' => [
        'tags' => $this->entityTypeManager->getDefinition('node_type')->getListCacheTags(),
      ],
    ];

    $items = [];

    $types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    uasort($types, [$definition->getClass(), 'sort']);

    // Only use node types the user has access to.
    /** @var \Drupal\node\Entity\NodeType $type */
    foreach ($types as $type) {
      $access = $this->entityTypeManager->getAccessControlHandler('node')->createAccess($type->id(), NULL, [], TRUE);
      if ($access->isAllowed()) {
        $link = Link::createFromRoute($type->label(), 'node.add', ['node_type' => $type->id()]);
        $markup = [
          '#markup' => '<div class="node-add-option"><div class="node-add-option__icon node-add-option__icon--' . $type->id() . '"></div><div class="node-add-option__text"><div class="node-add-option__title">'
            . $link->toString() . '</div></div>'
            . '<div class="node-add-option__description">' . check_markup($type->getDescription(), 'basic_html') . '</div></div>',
        ];
        $items[$type->id()] = $markup;
      }
      $this->renderer->addCacheableDependency($build, $access);
    }

    $build['#items'] = $items;

    return $build;

  }

}
