<?php

namespace Drupal\whereabouts_system\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class WhereaboutsController.
 *
 * @package Drupal\whereabouts_system\Controller
 */
class WhereaboutsController extends ControllerBase {

  /**
   * Returns empty page (home page uses blocks).
   *
   * @return array
   *   Build array.
   */
  public function home() {
    return [];
  }

  /**
   * Returns empty page (dashboard uses blocks).
   *
   * @return array
   *   Build array.
   */
  public function adminDashboard() {
    return [];
  }

}
