<?php

namespace Drupal\whereabouts_system\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Whereabouts System settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'whereabouts_system_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['whereabouts_system.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('whereabouts_system.settings');

    $form['location'] = [
      '#type' => 'details',
      '#title' => $this->t('Default location'),
      '#open' => TRUE,
    ];

    $form['location']['location_info'] = [
      '#type' => 'markup',
      '#markup' => '<div class"description">'
        . $this->t('Default values for locations in forms, maps, etc.')
        . '</div>',
    ];
    $form['location']['default_city'] = [
      '#type' => 'textfield',
      '#title' => $this->t('City'),
      '#default_value' => $config->get('default_city'),
    ];
    $form['location']['default_state'] = [
      '#type' => 'textfield',
      '#title' => $this->t('State'),
      '#default_value' => $config->get('default_state'),
    ];

    if ($options = whereabouts_system_get_state_options(FALSE)) {
      $form['location']['use_us_state_options'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Use US State select list'),
        '#description' => $this->t('If checked, a select list of US States will be used for State fields.'),
        '#default_value' => $config->get('use_us_state_options'),
      ];
      if ($config->get('use_us_state_options')) {
        $form['location']['default_state']['#type'] = 'select';
        $form['location']['default_state']['#options'] = $options;
      }
    }
    else {
      $form['location']['use_us_state_options'] = [
        '#type' => 'hidden',
        '#value' => 0,
      ];
    }

    $form['location']['map'] = [
      '#type' => 'details',
      '#title' => $this->t('Map settings'),
      '#open' => TRUE,
    ];
    $form['location']['map']['map_info'] = [
      '#type' => 'markup',
      '#markup' => '<div class"description">'
        . $this->t('Set the default zoom level and center of the map.')
        . '</div>',
    ];
    $form['location']['map']['default_lat'] = [
      '#type' => 'number',
      '#title' => $this->t('Latitude'),
      '#default_value' => $config->get('default_lat'),
      '#step' => 'any',
    ];
    $form['location']['map']['default_lng'] = [
      '#type' => 'number',
      '#title' => $this->t('Longitude'),
      '#default_value' => $config->get('default_lng'),
      '#step' => 'any',
    ];
    $form['location']['map']['default_zoom'] = [
      '#type' => 'number',
      '#title' => $this->t('Zoom'),
      '#default_value' => $config->get('default_zoom'),
      '#min' => 0,
      '#max' => 20,
    ];

    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $fields = [
      'default_city',
      'default_state',
      'use_us_state_options',
      'default_lat',
      'default_lng',
      'default_zoom',
    ];
    $values = $form_state->getValues();
    $config = $this->config('whereabouts_system.settings');
    foreach ($fields as $field) {
      $config->set($field, $values[$field] ?? '');
    }
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
