<?php

namespace Drupal\whereabouts_system\EventSubscriber;

use Drupal\search_api\Query\QueryInterface;
use Drupal\search_api\Event\QueryPreExecuteEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Whereabouts System event subscriber.
 */
class WhereaboutsSystemSubscriber implements EventSubscriberInterface {

  /**
   * Query pre-execute.
   *
   * @param \Drupal\search_api\Event\QueryPreExecuteEvent $event
   *   Query object.
   */
  public function onSearchApiQueryPreExecute(QueryPreExecuteEvent $event) {
    // Need to update the processing level so that we DO NOT run highlighting
    // on search results. That would result in rendering for some reason and
    // would cause timeouts.
    $query = $event->getQuery();
    $query->setProcessingLevel(QueryInterface::PROCESSING_BASIC);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      'search_api.query_pre_execute.search_json' => ['onSearchApiQueryPreExecute'],
    ];
  }

}
