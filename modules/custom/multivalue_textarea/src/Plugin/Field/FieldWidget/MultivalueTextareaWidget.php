<?php

namespace Drupal\multivalue_textarea\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the 'multivalue_textarea' field widget.
 *
 * @FieldWidget(
 *   id = "multivalue_textarea",
 *   label = @Translation("Multivalue Textarea"),
 *   field_types = {"string"},
 * )
 */
class MultivalueTextareaWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'rows' => 5,
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['rows'] = [
      '#type' => 'number',
      '#title' => t('Number of rows'),
      '#default_value' => $this->getSetting('rows'),
      '#required' => TRUE,
      '#min' => 1,
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = t('Textarea rows: @rows', ['@rows' => $this->getSetting('rows')]);
    return $summary;
  }

  /**
   * This method is not really used because we're using a single textarea for
   * all items.
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    return [];
  }

  /**
   * Special handling to create a single form element for multiple values.
   */
  protected function formMultipleElements(FieldItemListInterface $items, array &$form, FormStateInterface $form_state) {

    $cardinality = $this->fieldDefinition->getFieldStorageDefinition()->getCardinality();

    if ($cardinality == 1) {
      // If cardinality is 1, just use the standard field process.
      return parent::formMultipleElements($items, $form, $form_state);
    }

    $description = [$this->getFilteredDescription()];

    // Set some help text based on the cardinality.
    switch ($cardinality) {
      case FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED:
        $description[] = $this->t('Enter one value per line.');
        break;

      default:
        $description[] = $this->t('Enter up to @num values, one per line.', ['@num' => $cardinality]);
        break;
    }

    $default_values = [];
    foreach ($items as $item) {
      $default_values[] = $item->value;
    }

    $elements = [];
    $elements[] = [
      '#type' => 'textarea',
      '#rows' => $this->getSetting('rows') ?? 5,
      '#title' => $this->fieldDefinition->getLabel(),
      '#description' => implode("<br>\n", array_filter($description)),
      '#required' => $this->fieldDefinition->isRequired(),
      '#default_value' => implode("\n", $default_values),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function extractFormValues(FieldItemListInterface $items, array $form, FormStateInterface $form_state) {

    $cardinality = $this->fieldDefinition->getFieldStorageDefinition()->getCardinality();
    if ($cardinality == 1) {
      // If cardinality is 1, just use the standard field process.
      parent::extractFormValues($items, $form, $form_state);
      return;
    }

    $field_name = $this->fieldDefinition->getName();

    // Extract the values from $form_state->getValues().
    $path = array_merge($form['#parents'], [$field_name]);
    $key_exists = NULL;
    $values = NestedArray::getValue($form_state->getValues(), $path, $key_exists);
    $values = preg_split("/\R/", $values[0] ?? '');

    if ($key_exists) {
      // Let the widget massage the submitted values.
      $values = $this->massageFormValues($values, $form, $form_state);

      // Assign the values and remove the empty ones.
      $items->setValue($values);
      $items->filterEmptyItems();

      $num_values = count($items->getValue());
      if ($cardinality > 0 && $num_values > $cardinality) {
        $form_state->setErrorByName($field_name, $this->t('The maximum number of values allowed for %field is @num.', ['%field' => $this->fieldDefinition->getLabel(), '@num' => $cardinality]));
      }

      // Put delta mapping in $form_state, so that flagErrors() can use it.
      $field_state = static::getWidgetState($form['#parents'], $field_name, $form_state);
      static::setWidgetState($form['#parents'], $field_name, $form_state, $field_state);
    }

  }

}
