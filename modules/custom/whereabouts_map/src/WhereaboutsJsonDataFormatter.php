<?php

namespace Drupal\whereabouts_map;

use Drupal\image\Entity\ImageStyle;

class WhereaboutsJsonDataFormatter {

  public function getRowData($node) {

    if (empty($node->field_geolocation->lng) && empty($node->field_geolocation->lat)) {
      return FALSE;
    }

    $thumbnail = '';
    if ($media = $node->field_media_featured_image->entity) {
      /** @var \Drupal\file\Entity\File $file */
      if  ($file = $media->field_media_image->entity) {
        $thumbnail = ImageStyle::load('map_thumbnail')->buildUrl($file->getFileUri());
      }
    }
    if (!$thumbnail) {
      $theme_path = base_path() . \Drupal::service('extension.list.theme')->getPath('roamer');
      switch ($node->bundle()) {
        case 'object':
          $thumbnail = $theme_path . '/images/pennant-padded.svg';
          break;
        case 'building':
        default:
          $thumbnail = $theme_path . '/images/landmark-padded.svg';
      }
    }

    $search_index_text = trim(strtolower($node->label()))
      . ' ' . whereabouts_system_build_street_address_text($node);
    $search_index_text = preg_replace('/([^\w]|_)+/', ' ', str_replace("'", '', $search_index_text));
    $search_index = array_values(array_filter(explode(' ', $search_index_text)));

    // Note: All escaping of HTML entities is done when this content is displayed.
    $data = [
      'type' => 'Point',
      'properties' => [
        'nid' => $node->id(),
        'type' => $node->type->entity->label(),
        'url' => $node->toUrl()->toString(),
        'title' => trim($node->label()),
        'search_index' => $search_index,
        'thumbnail' => $thumbnail,
        'construction_type' => $this->getReferenceIds($node->field_construction_type),
        'neighborhood' => $this->getReferenceIds($node->field_neighborhood),
        'use' => $this->getReferenceIds($node->field_use),
        'year_built' => $node->field_year_built->value,
        'year_demolished' => $node->field_year_demolished->value,
        'architect' => $this->getReferenceIds($node->field_architect),
        'builder' => $this->getReferenceIds($node->field_builder),
        'related_businesses' => $this->getReferenceIds($node->field_related_businesses),
        'related_people' => $this->getReferenceIds($node->field_related_people_ref),
        'address' => [
          'formatted' => $this->formatAddress($node),
          'city' => $node->field_city->value,
          'state' =>$node->field_state->value,
          'street_number' => $node->field_street_number->value,
          'street_name' => $this->getTermLabel($node->field_street_name),
          'cross_street' => $this->getTermLabel($node->field_cross_street),
        ]
      ],
      'geometry' => [
        'type' => 'Point',
        'coordinates' => [
          $node->field_geolocation->lat,
          $node->field_geolocation->lng,
        ],
      ],
    ];

    if ($node->bundle() == 'building') {
      $data['properties']['architectural_style'] = $this->getReferenceIds($node->field_architectural_style);
      $data['properties']['building_type'] = $this->getReferenceIds($node->field_building_type);
      $data['properties']['local_historic_district'] = $this->getReferenceIds($node->field_local_historic_district);
      $data['properties']['national_register'] = $this->getReferenceIds($node->field_national_register);
    }
    $data['properties'] = array_filter($data['properties']);
    //      ksm($data);

    return $data;

  }

  public function formatAddress($node) {
    $address = $node->field_street_number->value . " " . $this->getTermLabel($node->field_street_name);
    if ($node->field_city->value) {
      $address .= ", " . $node->field_city->value;
      if ($node->field_state->value) {
        $address .= " " . $node->field_state->value;
      }
    }
    else {
      if ($node->field_state->value) {
        $address .= ", " . $node->field_state->value;
      }
    }
    return $address;
  }

  public function getReferenceIds($field) {
    if (!$field) {
      return [];
    }
    $ids = [];
    /** @var \Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem $item */
    foreach ($field as $item) {
      $ids[] = $item->target_id;
    }
    return $ids;
  }

  public function getTermLabel($field) {
    if (!$field || !$field->entity) {
      return NULL;
    }
    return $field->entity->label();
  }

}
