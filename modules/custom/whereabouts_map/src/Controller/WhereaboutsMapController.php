<?php

namespace Drupal\whereabouts_map\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for whereabouts_map routes.
 */
class WhereaboutsMapController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {
    $config = $this->config('whereabouts_system.settings');
    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('<whereabouts-search-map default_lat="@lat" default_lng="@lng" default_zoom="@zoom"></whereabouts-search-map>',
        [
          '@lat' => $config->get('default_lat') ?? 0,
          '@lng' => $config->get('default_lng') ?? 0,
          '@zoom' => $config->get('default_zoom') ?? 13,
        ]
      ),
    ];

    $build['#attached']['library'][] = 'whereabouts_map/whereabouts_search_map';
    return $build;
  }

}
