<?php

namespace Drupal\whereabouts_map\Controller;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheableResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\Renderer;
use Drupal\whereabouts_map\WhereaboutsCachedJson;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Whereabouts routes that return cached JSON.
 */
class WhereaboutsCachedJsonController extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The render service.
   *
   * @var \Drupal\Core\Render\Renderer
   *   Renderer service.
   */
  protected Renderer $renderer;

  /**
   * The cached JSON service.
   *
   * @var \Drupal\whereabouts_map\WhereaboutsCachedJson
   *   Cached JSON service.
   */
  protected WhereaboutsCachedJson $cachedJson;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The render service.
   * @param \Drupal\whereabouts_map\WhereaboutsCachedJson $cached_json
   *   The cached JSON service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, Renderer $renderer, WhereaboutsCachedJson $cached_json) {
    $this->entityTypeManager = $entity_type_manager;
    $this->renderer = $renderer;
    $this->cachedJson = $cached_json;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): WhereaboutsCachedJsonController {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('renderer'),
      $container->get('whereabouts_map.cached_json')
    );
  }

  /**
   * Serve the cached map data JSON file if it exists, recreate it otherwise.
   *
   * @return \Drupal\Core\Cache\CacheableResponse
   */
  public function mapData(): CacheableResponse {
    return $this->serveFile('map.json', 'whereabouts_map_data', 'rest_export_full');
  }

  /**
   * Serve the cached map filters JSON file if it exists, recreate it otherwise.
   *
   * @return \Drupal\Core\Cache\CacheableResponse
   */
  public function mapFilters(): CacheableResponse {
    return $this->serveFile('filters.json', 'whereabouts_map_filters', 'rest_export_filters');
  }

  /**
   * Serve the cached map node filters JSON file if it exists, recreate it otherwise.
   *
   * @return \Drupal\Core\Cache\CacheableResponse
   */
  public function mapNodeFilters(): CacheableResponse {
    return $this->serveFile('node-filters.json', 'whereabouts_map_node_filters', NULL);
  }

  /**
   * Serve the cached file if it exists, recreate it otherwise.
   *
   * @param $filename
   *   The filename to serve.
   * @param $view_name
   *   The name of the View.
   * @param $display
   *   The View display.
   *
   * @return \Drupal\Core\Cache\CacheableResponse
   *   The response JSON.
   */
  public function serveFile($filename, $view_name, $display): CacheableResponse {

    $output = NULL;
    $build = [];

    $context = new RenderContext();
    /** @var \Drupal\Core\Cache\CacheableDependencyInterface $result */
    $result = $this->renderer->executeInRenderContext($context, function () use (&$build, &$output, $filename, $view_name, $display) {
      // Get JSON from the service.
      $output = $this->cachedJson->get($filename, $view_name, $display, $this->renderer);
      // Set cache tag.
      $build['#cache']['tags'][] = 'whereabouts_cached_json:' . $view_name . ':' . $display;
    });

    // Handle any bubbled cacheability metadata.
    if (!$context->isEmpty()) {
      $bubbleable_metadata = $context->pop();
      BubbleableMetadata::createFromObject($result)
        ->merge($bubbleable_metadata);
    }

    $response = new CacheableResponse();
    $response->setContent($output);
    $response->addCacheableDependency(CacheableMetadata::createFromRenderArray($build));
    $response->headers->set('Content-Type', 'application/json');

    return $response;

  }

}
