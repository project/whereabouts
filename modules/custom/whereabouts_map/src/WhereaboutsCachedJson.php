<?php

namespace Drupal\whereabouts_map;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Cache\Cache;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\State\State;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\views\Views;

/**
 * WhereaboutsCachedJson service.
 */
class WhereaboutsCachedJson {

  use StringTranslationTrait;

  /**
   * The directory where the cached JSON will be saved / read from.
   * @var string
   *   The directory.
   */
  public const CACHE_DIRECTORY = 'public://cached-data';

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\State
   */
  protected State $state;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $database;

  /**
   * Constructs a WhereaboutsCachedJson object.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\Core\State\State $state
   *   The file system service.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(RendererInterface $renderer, FileSystemInterface $file_system, State $state, Connection $database) {
    $this->renderer = $renderer;
    $this->fileSystem = $file_system;
    $this->state = $state;
    $this->database = $database;
  }

  /**
   * Return the JSON from the file, creating it if necessary.
   *
   * @param $filename
   *   The filename to save.
   * @param $view_name
   *   The name of the View.
   * @param $display
   *   The View display.
   * @param $renderer
   *   Renderer service.
   *
   * @return string
   */
  public function get($filename, $view_name, $display, $renderer = NULL): string {
    if (!$renderer) {
      $renderer =  $this->renderer;
    }
    $full_path = self::CACHE_DIRECTORY . '/' . $filename;
    if (!file_exists($full_path)) {
      $output = $this->create($filename, $view_name, $display, $renderer);
    }
    else {
      // Just get the JSON file.
      $output = file_get_contents($full_path);
    }
    return $output;
  }

  /**
   * Create the JSON file and save it.
   *
   * @param $filename
   *   The filename to save.
   * @param $view_name
   *   The name of the View.
   * @param $display
   *   The View display.
   * @param $renderer
   *   Renderer service.
   *
   * @return \Drupal\Component\Render\MarkupInterface
   */
  public function create($filename, $view_name, $display, $renderer = NULL): MarkupInterface {

    if (!$renderer) {
      $renderer =  $this->renderer;
    }

    $full_path = self::CACHE_DIRECTORY . '/' . $filename;

    if (!file_exists(self::CACHE_DIRECTORY)) {
      // Create directory.
      $directory = self::CACHE_DIRECTORY;
      $this->fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);
    } // Directory exists?

    // Give us some more time.
    set_time_limit(120);

    if ($view_name == 'whereabouts_map_node_filters') {
      // Not a view-- use custom SQL.
      $build = [
        '#markup' => $this->getMapNodeFilters(),
      ];
    }
    else {
      // Render the View.
      $view = Views::getView($view_name);
      $view->setDisplay($display);
      $build = $view->render();
    }
    $output = $renderer->renderRoot($build);

    // Set the cache key for this file.
    $cids = $this->state->get('whereabouts_map.cached_json_cids');
    $cids[$filename] = md5($output);
    $this->state->set('whereabouts_map.cached_json_cids', $cids);

    // Save the JSON.
    file_put_contents($full_path, $output);

    // Invalidate the cache for this view/display.
    Cache::invalidateTags(['whereabouts_cached_json:' . $view_name . ':' . $display]);

    return $output;

  }

  public function getUrl($filename, $node = NULL) {

    $map = [
      'map.json' => 'whereabouts_map.cached_map_data',
      'filters.json' => 'whereabouts_map.cached_map_filters',
      'tour.json' => 'view.whereabouts_map_data.rest_export_tour',
      'node-filters.json' => 'whereabouts_map.cached_map_node_filters',
    ];

    $route_name = $map[$filename] ?? NULL;
    if (!$route_name) {
      return NULL;
    }

    $options = [];
    if ($node) {
      // Set cache ID based on the last update timestamp.
      $cid = md5($node->getChangedTime());
      $options['arg_0'] = $node->id();
    }
    else {
      // Get the cache key for this file.
      $cids = $this->state->get('whereabouts_map.cached_json_cids');
      $cid = $cids[$filename] ?? '';
    }

    $url = Url::fromRoute($route_name, $options, ['query' => ['cid' => $cid]]);
    return $url->toString();

  }

  private function getMapNodeFilters() {

    $filters = [
      'related_businesses' => [
        'table' => 'node__field_related_businesses',
        'title' => $this->t('Related Businesses / Organizations'),
        'field_name' => 'field_related_businesses_target_id',
      ],
      'related_people' => [
        'table' => 'node__field_related_people_ref',
        'title' => $this->t('Related People'),
        'field_name' => 'field_related_people_ref_target_id',
      ],
      'architect' => [
        'table' => 'node__field_architect',
        'title' => $this->t('Architect'),
        'field_name' => 'field_architect_target_id',
      ],
      'builder' => [
        'table' => 'node__field_builder',
        'title' => $this->t('Builder'),
        'field_name' => 'field_builder_target_id',
      ],
    ];

    $n = 0;
    $sql = [];
    foreach ($filters as $key => $info) {
      $n++;
      $alias = 'n' . $n;
      $sql[] = "SELECT {$alias}.nid, {$alias}.title, '{$key}' AS filter_type
        FROM {node_field_data} {$alias}
        INNER JOIN {{$info['table']}} {$info['table']} ON {$info['table']}.{$info['field_name']} = {$alias}.nid ";
    } // Loop thru filters.

    $full_sql = implode("\nUNION\n", $sql)
      . "ORDER BY filter_type ASC, title ASC";

    $results = $this->database->query($full_sql);

    $last_filter_type = NULL;
    $data = [];
    foreach ($results as $row) {
      if ($row->filter_type != $last_filter_type) {
        $data[$row->filter_type] = [
          'id' => $row->filter_type,
          'label' => $filters[$row->filter_type]['title'],
          'values' => [],
        ];
        $last_filter_type = $row->filter_type;
      }
      $data[$row->filter_type]['values'][] = [
        'id' => $row->nid,
        'label' => $row->title,
      ];
    }

    return json_encode(['filters' => array_values($data)]);

  }

}
