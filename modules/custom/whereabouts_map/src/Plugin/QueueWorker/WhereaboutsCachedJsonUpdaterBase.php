<?php

namespace Drupal\whereabouts_map\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\whereabouts_map\WhereaboutsCachedJson;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines 'whereabouts_map_cached_json_updater' queue worker.
 *
 * @QueueWorker(
 *   id = "whereabouts_map_cached_json_updater",
 *   title = @Translation("WhereaboutsCachedJsonUpdater"),
 *   cron = {"time" = 60}
 * )
 */
class WhereaboutsCachedJsonUpdaterBase extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The cached JSON service.
   *
   * @var \Drupal\whereabouts_map\WhereaboutsCachedJson
   *   Cached JSON service.
   */
  protected WhereaboutsCachedJson $cachedJson;

  /**
   * WhereaboutsCachedJsonUpdater constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\whereabouts_map\WhereaboutsCachedJson $cached_json
   *   The cached JSON service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, WhereaboutsCachedJson $cached_json) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->cachedJson = $cached_json;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('whereabouts_map.cached_json')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $this->cachedJson->create($data['filename'], $data['view_name'], $data['display']);
  }

}
