<?php

namespace Drupal\whereabouts_map\Plugin\QueueWorker;

/**
 * Defines 'whereabouts_map_cached_json_updater__map_data' queue worker.
 *
 * @QueueWorker(
 *   id = "whereabouts_map_cached_json_updater__map_data",
 *   title = @Translation("WhereaboutsCachedJsonUpdaterMapData"),
 *   cron = {"time" = 60}
 * )
 */
class WhereaboutsCachedJsonUpdaterMapData extends WhereaboutsCachedJsonUpdaterBase {}
