<?php

namespace Drupal\whereabouts_map\Plugin\QueueWorker;

/**
 * Defines 'whereabouts_map_cached_json_updater__filter_data' queue worker.
 *
 * @QueueWorker(
 *   id = "whereabouts_map_cached_json_updater__filter_data",
 *   title = @Translation("WhereaboutsCachedJsonUpdaterFilterData"),
 *   cron = {"time" = 60}
 * )
 */
class WhereaboutsCachedJsonUpdaterFilterData extends WhereaboutsCachedJsonUpdaterBase {}
