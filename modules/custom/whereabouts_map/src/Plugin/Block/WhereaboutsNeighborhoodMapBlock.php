<?php

namespace Drupal\whereabouts_map\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block for display on the search.
 *
 * @Block(
 *   id = "whereabouts_neighborhood_map_block",
 *   admin_label = @Translation("Whereabouts Neighborhood Map Block"),
 *   category = @Translation("Whereabouts Map")
 * )
 */
class WhereaboutsNeighborhoodMapBlock extends BlockBase implements ContainerFactoryPluginInterface{


  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new MirademDiningBlock instance.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $build = [
      '#cache' => [
        'tags' => [],
        'contexts' => ['url'],
      ],
    ];


    $config = $this->configFactory->get('whereabouts_system.settings');


    $build['content'] = [
      '#markup' => $this->t('<whereabouts-neighborhood-map  center_lat="@lat" center_lng="@lng" zoom="@zoom"></whereabouts-neighborhood-map>', [
        '@lat' => $config->get('default_lat') ?? 0,
        '@lng' => $config->get('default_lng') ?? 0,
        '@zoom' => $config->get('default_zoom') ?? 13,
      ]),
    ];
    $build['#attached']['library'][] = 'whereabouts_map/whereabouts_neighborhood_map';

    return $build;
  }


}
