<?php

namespace Drupal\whereabouts_map\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block for the whereabouts map with geo search.
 *
 * @Block(
 *   id = "whereabouts_search_map_block",
 *   admin_label = @Translation("Whereabouts Search Map Block"),
 *   category = @Translation("Whereabouts Map")
 * )
 */
class WhereaboutsSearchMapBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new MirademDiningBlock instance.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $config = $this->configFactory->get('whereabouts_system.settings');
    $build['content'] = [
      '#markup' => $this->t('<whereabouts-search-map default_lat="@lat" default_lng="@lng" default_zoom="@zoom"></whereabouts-search-map>',
        [
          '@lat' => $config->get('default_lat') ?? 0,
          '@lng' => $config->get('default_lng') ?? 0,
          '@zoom' => $config->get('default_zoom') ?? 13,
        ]
      ),
    ];



    $build['#attached']['library'][] = 'whereabouts_map/whereabouts_search_map';
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $form = parent::blockForm($form, $form_state);
    $form['open_sidebar_default'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Open sidebar on load?'),
      '#description' => $this->t('Should the search sidebar be open on page load?'),
      '#default_value' => $this->configuration['open_sidebar_default'] ?? TRUE,
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['open_sidebar_default'] = $form_state->getValue('open_sidebar_default');
    $this->configuration['default_bounds'] = $form_state->getValue('default_bounds');
  }

}
