<?php

namespace Drupal\whereabouts_map\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block for display on tours to show each of the stops on a map.
 *
 * @Block(
 *   id = "whereabouts_tour_map_block",
 *   admin_label = @Translation("Whereabouts Tour Map Block"),
 *   category = @Translation("Whereabouts Map")
 * )
 */
class WhereaboutsTourMapBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new MirademDiningBlock instance.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
//    /data/tour/18052/tour.json

    $build = [
      '#cache' => [
        'tags' => [],
        'contexts' => ['url'],
      ],
    ];

    if (!$node = \Drupal::routeMatch()->getParameter('node')) {
      // Can't get the node from URL. Maybe it's a preview?
      $node = \Drupal::routeMatch()->getParameter('node_preview');
    }

    $got_locations = FALSE;
    if ($node instanceof NodeInterface) {
      $build['#cache']['tags'][] = 'node:' . $node->id();
      $got_locations = whereabouts_system_tour_has_mappable_stops($node);
    } // Got a node?

    if ($got_locations) {
      $config = $this->configFactory->get('whereabouts_system.settings');
      $build['content'] = [
        '#markup' => $this->t('<whereabouts-tour-map class="whereabouts-tour-map-block" current_stop="@current_stop" center_lat="@lat" center_lng="@lng" zoom="@zoom"></whereabouts-tour-map>', [
          '@current_stop' => "",
          '@lat' => $config->get('default_lat') ?? 0,
          '@lng' => $config->get('default_lng') ?? 0,
          '@zoom' => $config->get('default_zoom') ?? 13,
        ]),
      ];
      $build['#attached']['library'][] = 'whereabouts_map/whereabouts_tour_map';
    }

    return $build;
  }


//  /**
//   * {@inheritdoc}
//   */
//  public function blockForm($form, FormStateInterface $form_state) {
//
//    $form = parent::blockForm($form, $form_state);
//    $form['open_sidebar_default'] = array(
//      '#type' => 'checkbox',
//      '#title' => $this->t('Open Sidebar on load?'),
//      '#description' => $this->t('Should the search sidebar be open on page load?'),
//      '#default_value' => $this->configuration['open_sidebar_default'] ?? TRUE,
//    );
//
//    return $form;
//  }
//
//  /**
//   * {@inheritdoc}
//   */
//  public function blockSubmit($form, FormStateInterface $form_state) {
//    $this->configuration['open_sidebar_default'] = $form_state->getValue('open_sidebar_default');
//  }

}
