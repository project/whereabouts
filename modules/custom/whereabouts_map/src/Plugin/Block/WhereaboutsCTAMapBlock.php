<?php

namespace Drupal\whereabouts_map\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a block for display on the search.
 *
 * @Block(
 *   id = "whereabouts_cta_map_block",
 *   admin_label = @Translation("Whereabouts CTA Map Block"),
 *   category = @Translation("Whereabouts Map")
 * )
 */
class WhereaboutsCTAMapBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build['content'] = [
      '#markup' => $this->t('<whereabouts-cta-map cta_link="@cta_link" cta_text="@cta_text" default_bounds_str="@default_bounds"></whereabouts-cta-map>', [
        '@cta_text' => $this->configuration['cta_text'],
        '@cta_link' => $this->configuration['cta_link'],
        '@default_bounds' => $this->configuration['default_bounds']
      ]),
    ];
    $build['#attached']['library'][] = 'whereabouts_map/whereabouts_cta_map';

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $form['cta_text'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('CTA text'),
      '#description' => $this->t('The text that displays on the button below the CTA map on the search page. For example, "Go to Geographical Building Search"'),
      '#default_value' => $this->configuration['cta_text'] ?? '',
    ];
    $form['cta_link'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('CTA link'),
      '#description' => $this->t('The destination page for the CTA. For example, /search/map'),
      '#default_value' => $this->configuration['cta_link'] ?? '/search/map',
    ];
    $form['default_bounds'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Default bounds'),
      '#description' => $this->t('What should the default bounds be for the map? For example, <pre>[[36.0636, -78.9875], [35.9532, -78.8122]]</pre> is the area centered on Durham, NC.'),
      '#default_value' => $this->configuration['default_bounds'] ?? '[[36.0636, -78.9875], [35.9532, -78.8122]]',
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['cta_text'] = $form_state->getValue('cta_text');
    $this->configuration['cta_link'] = $form_state->getValue('cta_link');
    $this->configuration['default_bounds'] = $form_state->getValue('default_bounds');
  }

}
