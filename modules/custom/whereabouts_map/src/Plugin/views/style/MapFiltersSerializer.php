<?php

namespace Drupal\whereabouts_map\Plugin\views\style;


use Drupal\Component\Utility\Html;
use Drupal\image\Entity\ImageStyle;
use Drupal\rest\Plugin\views\style\Serializer;


/**
 * The style plugin for Map filters.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "whereabouts_map_filters_serializer",
 *   title = @Translation("Whereabouts map filters"),
 *   help = @Translation("Serializes views filter data for Whereabouts Maps."),
 *   display_types = {"data"}
 * )
 */
class MapFiltersSerializer extends Serializer {

  /**
   * {@inheritdoc}
   */
  public function render() {

    $rows = [];

    // If the Data Entity row plugin is used, this will be an array of entities
    // which will pass through Serializer to one of the registered Normalizers,
    // which will transform it to arrays/scalars. If the Data field row plugin
    // is used, $rows will not contain objects and will pass directly to the
    // Encoder.
    foreach ($this->view->result as $row_index => $row) {

      /** @var \Drupal\taxonomy\Entity\Term $term */
      $term = $row->_entity;

      if (empty($rows[$term->bundle()])) {
        $rows[$term->bundle()] = [
          'label' => Html::escape($term->vid->entity->label()),
          'id' => $term->bundle(),
          'values' => [],
        ];
      } // Vocabulary-level values set yet?

      $rows[$term->bundle()]['values'][$row_index] = [
        'id' => $term->id(),
        'label' => Html::escape(trim($term->label())),
      ];

    } // Loop thru rows.

    $terms = [];
    foreach ($rows as $vid => $data) {
      // Re-index the values.
      $data['values'] = array_values($data['values']);
      $terms[] = $data;
    }

    $rows = [
      'filters' => $terms,
    ];

    // Get the content type configured in the display or fallback to the
    // default.
    if ((empty($this->view->live_preview))) {
      $content_type = $this->displayHandler->getContentType();
    }
    else {
      $content_type = !empty($this->options['formats']) ? reset($this->options['formats']) : 'json';
    }
    return $this->serializer->serialize($rows, $content_type, ['views_style_plugin' => $this]);

  }

}
