<?php

namespace Drupal\whereabouts_map\Plugin\views\style;

use Drupal\rest\Plugin\views\style\Serializer;

/**
 * The style plugin for Map data.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "whereabouts_map_data_serializer",
 *   title = @Translation("Whereabouts map data"),
 *   help = @Translation("Serializes views row data for Whereabouts Maps."),
 *   display_types = {"data"}
 * )
 */
class MapDataSerializer extends Serializer {

  /**
   * {@inheritdoc}
   */
  public function render() {

    $rows = [];
    $is_tour = FALSE;

    /** @var \Drupal\whereabouts_map\WhereaboutsJsonDataFormatter $json_data_formatter */
    $json_data_formatter = \Drupal::service('whereabouts_map.json_data_formatter');

    // If the Data Entity row plugin is used, this will be an array of entities
    // which will pass through Serializer to one of the registered Normalizers,
    // which will transform it to arrays/scalars. If the Data field row plugin
    // is used, $rows will not contain objects and will pass directly to the
    // Encoder.
    foreach ($this->view->result as $row_index => $row) {

      $this->view->row_index = $row_index;

      /** @var \Drupal\node\Entity\Node $node */
      $node = $row->_entity;
      if ($node->bundle() == 'tour') {
        $node = $row->_relationship_entities['field_location'];
        $is_tour = TRUE;
      }

      if (!$data = $json_data_formatter->getRowData($node)) {
        continue;
      }

      if ($is_tour) {
        $data['properties']['tour_index'] = $row->index;
      }

      $rows[] = $data;

    }
    unset($this->view->row_index);

    $rows = [
      'type' => 'FeatureCollection',
      'features' => $rows,
    ];

    // Get the content type configured in the display or fallback to the
    // default.
    if ((empty($this->view->live_preview))) {
      $content_type = $this->displayHandler->getContentType();
    }
    else {
      $content_type = !empty($this->options['formats']) ? reset($this->options['formats']) : 'json';
    }
    return $this->serializer->serialize($rows, $content_type, ['views_style_plugin' => $this]);
  }

}
