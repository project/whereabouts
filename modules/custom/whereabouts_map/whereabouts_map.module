<?php

/**
 * @file
 * Primary module hooks for whereabouts_map module.
 *
 * @DCG
 * This file is no longer required in Drupal 8.
 * @see https://www.drupal.org/node/2217931
 */

use Drupal\Core\Entity\EntityInterface;

/**
 * Implements hook_entity_insert().
 */
function whereabouts_map_entity_insert(EntityInterface $entity) {
  switch ($entity->getEntityTypeId()) {
    case 'taxonomy_term':
      whereabouts_map_queue_map_filters_json_update($entity);
      break;
    case 'node':
      whereabouts_map_queue_map_data_json_update($entity);
      break;
  }
}

/**
 * Implements hook_entity_update().
 */
function whereabouts_map_entity_update(EntityInterface $entity) {
  switch ($entity->getEntityTypeId()) {
    case 'taxonomy_term':
      whereabouts_map_queue_map_filters_json_update($entity);
      break;
    case 'node':
      whereabouts_map_queue_map_data_json_update($entity);
      break;
  }

}

/**
 * Implements hook_entity_delete().
 */
function whereabouts_map_entity_delete(EntityInterface $entity) {
  switch ($entity->getEntityTypeId()) {
    case 'taxonomy_term':
      whereabouts_map_queue_map_filters_json_update($entity);
      break;
    case 'node':
      whereabouts_map_queue_map_data_json_update($entity);
      break;
  }
}

function whereabouts_map_queue_map_filters_json_update(EntityInterface $entity) {
  switch ($entity->bundle()) {
    case 'architectural_style':
    case 'building_type':
    case 'business_type':
    case 'construction_type':
    case 'local_historic_district':
    case 'national_register':
    case 'neighborhood':
    case 'use':
      // Update required for these vocabularies-- queue it up.
      $item = [
        'filename' => 'filters.json',
        'view_name' => 'whereabouts_map_filters',
        'display' => 'rest_export_filters'
      ];
      whereabouts_map_queue_single_item('whereabouts_map_cached_json_updater__filter_data', $item);
      break;
    default:
      // No update needed.
  }
}

function whereabouts_map_queue_map_data_json_update(EntityInterface $entity) {
  switch ($entity->bundle()) {
    case 'building':
    case 'object_of_interest':
      // Update required for these content types-- queue it up.
      $item = [
        'filename' => 'map.json',
        'view_name' => 'whereabouts_map_data',
        'display' => 'rest_export_full'
      ];
      whereabouts_map_queue_single_item('whereabouts_map_cached_json_updater__map_data', $item);
      break;
    default:
      // No update needed.
  }
}

function whereabouts_map_queue_single_item($queue_name, $item) {

  // Check if there's already an update queued.
  $query = \Drupal::database()->select('queue', 'q');
  $query->addField('q', 'item_id');
  $query->condition('name', $queue_name);
  $query->condition('expire', 0);
  if ($item_id = $query->execute()->fetchField()) {
    // Queue item already exists.
    return;
  }

  // No queue item-- create one.
  /** @var \Drupal\Core\Queue\QueueFactory $queue_factory */
  $queue_factory = \Drupal::service('queue');
  /** @var \Drupal\Core\Queue\QueueInterface $queue */
  $queue = $queue_factory->get($queue_name);
  $queue->createItem($item);
}


/**
 * Implements hook_library_info_alter().
 */
function whereabouts_map_library_info_alter(&$libraries, $extension) {
//  ksm('$libraries, $extension', $libraries, $extension);
  if ($extension == 'whereabouts_map') {
    foreach ($libraries as $library_name => $library) {
      if ($libraries[$library_name]['js']) {
        foreach ($libraries[$library_name]['js'] as $file => $options) {
          $libraries[$library_name]['js'][$file]['preprocess'] = FALSE;
        }
      }
    }
  }
}
