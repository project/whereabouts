import Vue from 'vue'
import App from './App.vue'

Vue.config.devtools = true
Vue.config.productionTip = false

import {updateScopeId} from "./utilities";

Vue.directive('scope', {
  bind: updateScopeId,
});

new Vue({
  render: h => h(App)
}).$mount('#vue-container')

