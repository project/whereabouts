export const setScopeRecursively = function(targetEl, scopeId) {
  // This function will recursively travel down from the targetEl and apply the scopeId as a data attribute for CSS scoping.
  // scopeId should be a string (e.g. v-123f1123a)
  try {
    targetEl.setAttribute(`data-${scopeId}`,"")
  } catch(e) {
    console.log(e)
  }

  // Recursively call setScopeRecursively for any children.
  if (targetEl.hasChildNodes()) {
    const list = targetEl.children
    for (var item of list) {
      setScopeRecursively(item, scopeId)
    }
  }
}


export function updateScopeId(el, __, vNode) {
  const vm = vNode.context
  const scopeId = vm & vm.$options._scopeId
  if (scopeId) {
    vm.$nextTick(() => { // wait till DOM was updated
      setScopeRecursively(el, `v-${scopeId}`)
    })
  }
}

export const handleErrors = function(response) {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}

export const parseYearInt = function(year, first_or_last = 'first') {
  if (!year) {
    return undefined;
  }
  if (year.split('-').length > 1) {
    return parseInt(year.split('-')[first_or_last === "first" ? 0 : 1])
  }
  return parseInt(year);
}

export const debounce = function(func, wait, immediate) {
  let timeout;
  return function() {
    let context = this, args = arguments;
    let later = function() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    let callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};
