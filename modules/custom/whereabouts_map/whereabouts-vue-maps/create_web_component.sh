#!/bin/bash

npx vue-cli-service build --target wc src/components/CTAMap.vue --inline-vue --name whereabouts-cta-map
npx vue-cli-service build --no-clean --target wc src/components/WhereaboutsSearch.vue --inline-vue --name whereabouts-search-map
npx vue-cli-service build --no-clean --target wc src/components/TourMap.vue --inline-vue --name whereabouts-tour-map
npx vue-cli-service build --no-clean --target wc src/components/NeighborhoodMap.vue --inline-vue --name whereabouts-neighborhood-map
